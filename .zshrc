# Path to your oh-my-zsh installation.
export ZSH="/opt/zsh/oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
#ZSH_THEME="refined"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# Caution: this setting can cause issues with multiline prompts (zsh 5.7.1 and newer seem to work)
# See https://github.com/ohmyzsh/ohmyzsh/issues/5765
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  history
  git
  history-substring-search
  autojump
)

# maybe ~/bin is not already in the PATH
export PATH=$HOME/bin:$HOME/.local/bin:/usr/local/sbin:$PATH
export PATH="/usr/local/opt/util-linux/bin:$PATH"
export PATH="/usr/local/opt/util-linux/sbin:$PATH"
export PATH="/usr/local/opt/findutils/libexec/gnubin:$PATH"

# Disable compinit check because it is done manually later
ZSH_DISABLE_COMPFIX=true

# brew completions
FPATH="$(brew --prefix)/share/zsh/site-functions:${FPATH}"

source $ZSH/oh-my-zsh.sh

# User configuration

# Complete ./ and ../
zstyle ':completion:*' special-dirs true

# Sort completions by modification time
zstyle ':completion:*' file-sort access

trusted_users=(max tw)
compinit_check=""
# If compaudit finds potentially insecure files or directories ...
if ! compaudit > /dev/null 2>&1; then
    # ... check if any of them do not belong to the trusted users.
    if compaudit 2> /dev/null | xargs ls -ld | awk '{print $3}' \
    | python3 -c 'import sys; sys.exit(len([u for u in sys.stdin if u.strip() not in sys.argv[1:]]))' "${trusted_users[@]}"
    then
        # If no untrusted users are found then apply -u to use all insecure entries.
        compinit_check="-u"
    fi
fi
autoload -U compinit && compinit $compinit_check
autoload bashcompinit && bashcompinit

# micro
export EDITOR=/usr/bin/nano
which micro > /dev/null 2>&1 && export EDITOR=$(which micro)
export VISUAL=$EDITOR

# powerline
function powerline_precmd() {
    PS1="$(powerline-go -error $? -jobs ${${(%):%j}:-0})"

    # Uncomment the following line to automatically clear errors after showing
    # them once. This not only clears the error for powerline-go, but also for
    # everything else you run in that shell. Don't enable this if you're not
    # sure this is what you want.

    #set "?"
}

function install_powerline_precmd() {
  for s in "${precmd_functions[@]}"; do
    if [ "$s" = "powerline_precmd" ]; then
      return
    fi
  done
  precmd_functions+=(powerline_precmd)
}

if [ "$TERM" != "linux" ]; then
    install_powerline_precmd
fi

# Go
#export GOROOT=$HOME/Tools/go
#export GOPATH=$HOME/Projects/go
#export PATH=$GOROOT/bin:$GOPATH/bin:$PATH

# direnv
type direnv > /dev/null 2>&1 && eval "$(direnv hook zsh)"

# Python
export PYTHONWARNINGS=$PYTHONWARNINGS,"ignore:DEPRECATION::pip._internal.cli.base_command"
type pip > /dev/null 2>&1 && eval "$(pip completion --zsh)"
type pip3 > /dev/null 2>&1 && eval "$(pip3 completion --zsh)"
type pipenv > /dev/null 2>&1 && eval "$(pipenv --completion)"

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

# iTerm2
[[ -e ~/.iterm2_shell_integration.zsh ]] && source ~/.iterm2_shell_integration.zsh

# brew
[[ -e /home/linuxbrew/.linuxbrew/bin/brew ]] && eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
[[ -e /opt/homebrew/bin/brew ]] && eval "$(/opt/homebrew/bin/brew shellenv)"

# asdf
[[ -e $(brew --prefix asdf)/libexec/asdf.sh ]] && source $(brew --prefix asdf)/libexec/asdf.sh

setopt interactivecomments

# grep
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

# ls
alias ll='ls -l'
alias la='ls -A'
alias lah='ls -lah'
alias l='ls -CF'
alias rs='reset'

alias isodate='date -Iseconds'
alias nano=$EDITOR

type thefuck > /dev/null 2>&1 && eval $(thefuck --alias)

# work stuff
#type ~/.local/bin/aws-okta > /dev/null 2>&1 && complete -o nospace -C ~/.local/bin/aws-okta aws-okta
