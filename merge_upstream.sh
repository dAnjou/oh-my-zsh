#!/bin/bash

git remote add upstream https://github.com/ohmyzsh/ohmyzsh.git
git fetch upstream &&
git checkout master &&
git merge upstream/master &&
git push origin master
